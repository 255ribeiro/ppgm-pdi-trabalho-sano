 ENVI Output of ROIs (5.0) [Mon Mar 14 19:16:29 2022]
 Number of ROIs: 5
 File Dimension: 7581 x 7701

 ROI name: Urbano
 ROI rgb value: {255, 0, 0}
 ROI npts: 934

 ROI name: Vegetacao Grande Porte
 ROI rgb value: {0, 255, 0}
 ROI npts: 1129

 ROI name: Veretacao Esparca
 ROI rgb value: {0, 0, 255}
 ROI npts: 662

 ROI name: Pastagem
 ROI rgb value: {255, 255, 0}
 ROI npts: 2340

 ROI name: Agricultura
 ROI rgb value: {0, 255, 255}
 ROI npts: 1518
