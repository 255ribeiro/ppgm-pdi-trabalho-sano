# INFORMAÇÕES TRABALHO

## BANDAS DO LANDSAT

![landsat8_bands.png](./landsat8_bands.png)


## INDICES DE VEGETAÇÃO

NDVI = (FLOAT(B5) - FLOAT(B4)) / (FLOAT(B5) + FLOAT(B4))

EVI = 2.5 * ((FLOAT(B5)-FLOAT(B4)) / (FLOAT(B5) + 6 * FLOAT(B4) - 7.5 * FLOAT(B2) + 1))

EVI2 = 2.5 * ((FLOAT(B5) - FLOAT(B4)) / (FLOAT(B5) + 2.4 * FLOAT(B4) + 1 ))
