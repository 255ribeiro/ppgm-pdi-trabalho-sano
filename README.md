# Links uteis

## Interface web GEE

[Google Earth Engine](https://earthengine.google.com/)

[GEE Code Editor](https://code.earthengine.google.com/)

[Docs](https://developers.google.com/earth-engine/)

## Crusos de Google Earth Engine

[Program Sam](https://www.youtube.com/watch?v=W2V_awzKDOg&list=PLivRXhCUgrZpCR3iSByLYdd_VwFv-3mfs)

[Luis Sadeck](https://www.youtube.com/watch?v=Dqjtoj9AJak&list=PLNFvG6bTA4NReWtgC93Mh9Tw1RNG4EBMP)

[Curso GEE Python](https://www.youtube.com/watch?v=wGjpjh9IQ5I&list=PLAxJ4-o7ZoPccOFv1dCwvGI6TYnirRTg3)


## Procura de imagens

[Earth explorer](https://earthexplorer.usgs.gov/)



----

e-mail Sano 

edson.sano@gmail.com

edson.sano@embrapa.br
